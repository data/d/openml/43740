# OpenML dataset: GOOG-Ticker-stock-data

https://www.openml.org/d/43740

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The stock prices dataset of  a ticker is a good start to slice and dice and good for forecasting of the stock prices.
The GOOG ticker data is taken
Content
Dataset is comprising of the below columns and each row having the days data, an year data is pulled into csv file.
Date   Open    High     Low   Close   Volume
Acknowledgements
Credits: The data is pulled from the Google  
Inspiration
Stock data  for forecasting stock prices.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43740) of an [OpenML dataset](https://www.openml.org/d/43740). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43740/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43740/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43740/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

